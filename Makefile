VERSION=0.1
IMAGE_BASE=build/artivis-diy-${VERSION}
PROXY_CACHE=

all: build-base

build-base: $(IMAGE_BASE).img

$(IMAGE_BASE).img: spec/base.vmdb
	@sudo http_proxy=$(PROXY_CACHE) scripts/build_image.sh $< $@

$(IMAGE_BASE)-%.img: $(IMAGE_BASE).img
	@sudo http_proxy=$(PROXY_CACHE) scripts/custom_$*.sh $<

# rules for custom targets
custom-zerow: $(IMAGE_BASE)-zerow.img

edit: $(IMAGE_BASE).img
	@sudo scripts/edit_image.sh $<

run: $(IMAGE_BASE).img
	@sudo scripts/run_image.sh $<

clean:
	-sudo rm build/*

help:
	@echo "ARTiVIS DIY Forest Surveillance Kit Image Builder"
	@echo ""
	@echo "Available targets:"
	@echo ""
	@echo "        build:  run disk image build process (default)"
	@echo "custom-TARGET:  customize the base image to run on TARGET"
	@echo "                possible tagets: zerow"
	@echo "         edit:  mount the disk image run a shell in it"
	@echo "          run:  boot the image using qemu"
	@echo "        clean:  cleanup everything"
	@echo ""
	@echo "Note: If you have apt-cacher-ng (or another similar proxy for deb repos)"
	@echo "you can call the build targets using:"
	@echo ""
	@echo "    make -e PROXY_CACHE=http://<proxy-hostame>:<proxy-port> <target>"
	@echo ""

.PHONY: custom-zerow build clean help
