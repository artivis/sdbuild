#! /bin/bash

# oneshot script to perform basic bare-metal config on first boot

#
# setup unique hostname
#

# generate a unique machine id from the processor serial number
HW_SUFFIX=$(cat /proc/cpuinfo | grep Serial | tr -d "\n" | tail -c 4)

MACHINE_ID=artivis-${HW_SUFFIX}

echo ${MACHINE_ID} > /etc/hostname
hostname -F /etc/hostname

# add it to the hosts file
sed -i "s/%ARTIVIS%/${MACHINE_ID}/g" /etc/hosts

# also use the machine id as the SSID for the wifi access point
sed -i "s/%ARTIVIS%/${MACHINE_ID}/g" /etc/hostapd/hostapd.conf

# resize root device (and reboot)
raspi-config nonint do_expand_rootfs
reboot
