
Image Builder for ARTIVIS DIY Forest Surveillance Kit
=====================================================

This repository contains a set of configuration files and scripts to help automate the creation of disk images for the [ARTIVIS DIY Forest Surveillance Kit](http://diy.artivis.net)

Supported devices
-----------------

We currently target and test on Raspbian-based devices (Raspberry Pi B, Raspberry Pi 2/3 and Raspberry Pi Zero W) but the build process should be easy to adapt to any other Debian-based distribution.

Features
--------

Beyond being a minimal Raspbian installation to run our video streaming server on, these disk images aim for:

 * easy first-time configuration
   * start in host AP mode if a wireless interface is present
   * hand out addresses via DHCP server on eth0
   * advertise a unique SSID and hostname (via avahi)
   * provide a web interface for basic configuration
 * easy streaming setup
   * local video stream out of the box
   * web interface to change streaming configuration

Requirements
------------

 * `vmdb2` to build the disk image (`qemu-user-static` is required for cross-building)
 * `qemu-system-arm` to run the image in an emulator
 * `systemd-nspawn` and `qemu-user-static` to launch an emulated  shell inside the image

Building
--------

The repository contains a Makefile that drives a set of scripts that take care of common tasks like building the image file and testing it under an emulator. Running `make help` will list the available targets. A typical workflow ususally consists of:

    make                     # (or make build) to create the base image
                             # (grab a cup of coffee, this takes a while)
    
    make edit                # to launch a container inside the new image
                             # (and for making manual tweaks to the image)
    
    make run                 # to try to boot the image under a raspi emulator
                             # (this is still WIP upstream and things like keyboard
                             # or mouse input seem to be missing)
    
    make custom-<target>     # where <target> is a specific machine variant
                             # that you want to customize the base image for
	                     # take a look in scripts/custom-<target>.sh to see
			     # what the available targets are and what customizations 
                             # are applied

    make clean               # to start over :)

Caching
-------

Using a tool like [apt-cacher-ng](https://www.unix-ag.uni-kl.de/~bloch/acng/) to cache repeated requests for the same debian packages from remote repositories can dramatically reduce the time and bandwidth required to build an image file. After you install and configure this proxy tool you can build an image file with:

    make -e PROXY_CACHE=http://<proxy-hostname>:<proxy-port>

and then go grab an expresso instead of a long tea.

Installing
----------

You can write the image to the SD card using

    ./scripts/burn_image.sh <image-file> <sd-card-device>

the script does some basic checks to make sure no disaster happens, but make sure the path to <image-file> and to <sd-card-device > are correct. If you are feeling squeamish about the possibility of accidentaly erasing your computer's hard drive you can (and should) use something like [Etcher](http://etcher.io).

Usage
-----

The [ARTiVIS Video Streaming Kit project repository](http://gitlab.com/artivis/video-streaming-kit/) contains high-level documentation on video streaming and how to use these images to create your own little media streaming servers.

Contributing
------------

We welcome all contributions. If you find a problem, or have an idea for improvement please [post an issue](https://gitlab.com/artivis/sdbuild/issues/new) or [create a merge request](https://gitlab.com/artivis/sdbuild/merge_requests/new)
