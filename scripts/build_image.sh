#! /bin/bash

SPEC=$1
TARGET=$2
BUILDER="vmdb2"
BUILD_ENV="LC_CTYPE=C.UTF-8 PATH="/usr/sbin:/sbin:${PATH}" http_proxy=${http_proxy}"
BASEDIR=$(dirname "$0")

source ${BASEDIR}/functions.sh

root_check
tool_check ${BUILDER}
file_param_check ${SPEC} "spec file"
param_check ${TARGET} "target filename"

env -i ${BUILD_ENV} ${BUILDER} --output ${TARGET} ${SPEC} --log stderr


