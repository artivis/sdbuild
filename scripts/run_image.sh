#! /bin/bash

IMAGE=$1
RUNNER="qemu-system-arm"
BASEDIR=$(dirname "$0")
BUILDDIR=$(dirname "$1")

source ${BASEDIR}/functions.sh

root_check
tool_check ${RUNNER}

# setup mounts
map_partitions ${IMAGE}
mount_partitions ${BUILDDIR}/rootfs

# run the image
${RUNNER} -machine raspi2,usb=on -kernel ${BOOTDIR}/kernel7.img -dtb ${BOOTDIR}/bcm2709-rpi-2-b.dtb -append "rw dwc_otg.lpm_enable=0 console=tty1 console=serial0,115200 root=/dev/mmcblk0p2 rootfstype=ext4 elevator=deadline rootwait" -drive file="${IMAGE}",if=sd,format=raw -monitor stdio

# cleanup
umount_partitions
unmap_partitions ${IMAGE}
