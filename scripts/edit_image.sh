#! /bin/bash

IMAGE=$1
RUNNER="systemd-nspawn"
BASEDIR=$(dirname "$0")
BUILDDIR=$(dirname "$1")

source ${BASEDIR}/functions.sh

root_check
tool_check ${RUNNER}

map_partitions ${IMAGE}
mount_partitions ${BUILDDIR}/rootfs

# enter the image
${RUNNER} -D ${ROOTDIR}

umount_partitions
unmap_partitions ${IMAGE}
