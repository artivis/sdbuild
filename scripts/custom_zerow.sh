#! /bin/bash -x

IMAGE=$1
BASEDIR=$(dirname "$0")
RUNNER="systemd-nspawn"

source ${BASEDIR}/functions.sh

root_check
tool_check ${RUNNER}
file_param_check $1

BUILDDIR=$(dirname "$1")
IMAGE_BASE=$(basename "$1" .img)
NEW_IMAGE=${BUILDDIR}/${IMAGE_BASE}-zerow.img

cp ${IMAGE} ${NEW_IMAGE}

CONFDIR=${BASEDIR}/../config/zerow

# setup mounts
map_partitions ${NEW_IMAGE}
mount_partitions ${BUILDDIR}/rootfs

#install extra deps
${RUNNER} -E http_proxy=${http_proxy} -D ${ROOTDIR} /bin/bash << EOS

 # install needed packages
 apt-get update
 apt-get -y install hostapd udhcpd
 
 touch /var/lib/misc/udhcpd.leases

 # make g_serial tty available
 ln -s /lib/systemd/system/getty@.service /etc/systemd/system/getty.target.wants/getty@ttyGS0.service

 # remove unneeded eth0 interface definition
 rm /etc/network/interfaces.d/eth0

 # cleanup
 apt-get clean
 rm -rf /var/lib/apt/lists

EOS

# install device-specific customizations
install -m 755 -o root -g root ${CONFDIR}/boot/* ${BOOTDIR}

install -m 644 -o root -g root ${CONFDIR}/net/udhcpd.conf ${ROOTDIR}/etc/udhcpd.conf

install -m 644 -o root -g root ${CONFDIR}/net/udhcpd.default ${ROOTDIR}/etc/default/udhcpd

install -m 755 -o root -g root ${CONFDIR}/net/udhcpd.init ${ROOTDIR}/etc/init.d/udhcpd

install -m 644 -o root -g root ${CONFDIR}/net/ap0 ${ROOTDIR}/etc/network/interfaces.d/ap0

install -m 644 -o root -g root ${CONFDIR}/net/hostapd.conf ${ROOTDIR}/etc/hostapd/hostapd.conf

# override base machine setup service
install -m 755 -o root -g root ${CONFDIR}/init/artivis-setup.sh ${ROOTDIR}/usr/sbin/

# setup AP to start on boot
install -m 644 -o root -g root ${CONFDIR}/init/artivis-ap.service ${ROOTDIR}/etc/systemd/system/artivis-ap.service

${RUNNER} -D ${ROOTDIR} /bin/bash << EOS

 ln -s /etc/systemd/system/artivis-ap.service /etc/systemd/system/multi-user.target.wants/artivis-ap.service

EOS

# cleanup
umount_partitions
unmap_partitions ${NEW_IMAGE}

