#
# Helper functions for build scripts
#

# basic function to get the script name
function script_name {
  echo $(basename "$0")
}

# check for root permissions
function root_check {
  if [[ ${EUID} -ne 0 ]]; then
    error_abort "this script must be run as root"
  fi
}

# check if a tool is available
# $1: command name (the actual name that should be called
#     to run the program)
function tool_check {
  if [ -z $(which ${1}) ]; then
    error_abort "tool ${1} is not installed"
  fi
}

# check if a parameter is a non-zero length string
# $1: parameter string to check
function param_check {
  if [ -z ${1} ]; then
    error_abort "missing parameter"
  fi
}

# check if a file parameter is valid
# $1: file path
function file_param_check {
  param_check $1
  if [ ! -f ${1} ]; then
    error_abort "missing file: ${1}"
  fi
}

# check if a directory parameter is valid
# $1: directory path
function dir_param_check {
  param_check $1
  if [ ! -d ${1} ]; then
    error_abort "missing dir: ${1}"
  fi
}

# check if a block device is available and is safe to write
function device_check {
  param_check $1
  if [ ! -b $1 ]; then
    error_abort "${1} is not a block device"
  fi
  DEV_MOUNTS=$(mount | grep ${1} | cut -d " " -f 3)
  for MOUNT_POINT in ${DEV_MOUNTS}; do
    if [ ${MOUNT_POINT} = '/' ] ||
       [ ${MOUNT_POINT} = '/home' ]; then
      error_abort "${1} has unsafe mount: ${MOUNT_POINT}"
    fi
  done
}

# map image partitions to loopback devices
# $1: path to image file
# sets $BOOT_PART and $ROOT_PART to appropriate
# /dev/mapper/* paths
function map_partitions {
  file_param_check ${1}
  LOOP_DEVS=$(kpartx -av ${1} | cut -d " " -f 3)
  for LOOP_DEV in ${LOOP_DEVS}; do
    BOOT_NODE=$(echo ${LOOP_DEV} | egrep "p1$")
    ROOT_NODE=$(echo ${LOOP_DEV} | egrep "p2$")
    if [ ! -z ${BOOT_NODE} ]; then
      BOOT_PART=/dev/mapper/${BOOT_NODE}
    elif [ ! -z ${ROOT_NODE} ]; then
      ROOT_PART=/dev/mapper/${ROOT_NODE}
    else
      error_abort "error mapping partitions"
    fi
  done
}

# cleanup partition maps (requires previous call
# to map_partitions)
# $1: path to image file
function unmap_partitions {
  file_param_check ${1}
  kpartx -d ${1}
}

# mount partitions (requires previous call to map_partitions)
# $1: path to mount partitions on
# sets $ROOTDIR and $BOOTDIR to the mounted filesystems paths
# of / and /boot
function mount_partitions {
  param_check ${1}
  param_check ${BOOT_PART}
  param_check ${ROOT_PART}
  ROOTDIR=${1}
  BOOTDIR=${ROOTDIR}/boot
  mkdir ${ROOTDIR}
  mount ${ROOT_PART} ${ROOTDIR} -o loop
  mount ${BOOT_PART} ${BOOTDIR} -o loop
}

# u(n)mount partitions (requires previous call to
# mount_partitions)
# $1: base path of mounted partitions
function umount_partitions {
  dir_param_check ${BOOTDIR}
  dir_param_check ${ROOTDIR}
  umount ${BOOTDIR}
  umount ${ROOTDIR}
  rmdir ${ROOTDIR}
}

# print an error message and quit the script
function error_abort {
  echo "$(script_name): [error] ${1}" 1>&2
  exit 1
}
