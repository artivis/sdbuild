#! /bin/bash

IMAGE=$1
SD_DEVICE=$2
CLONER="dd"
BASEDIR=$(dirname "$0")

source ${BASEDIR}/functions.sh

root_check
tool_check ${CLONER}
file_param_check ${IMAGE}
device_check ${SD_DEVICE}

${CLONER} if=${IMAGE} of=${SD_DEVICE} bs=64k oflag=dsync status=progress
